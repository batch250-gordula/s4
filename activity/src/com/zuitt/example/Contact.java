package com.zuitt.example;

public class Contact {

    // Defining properties/description
    private String name;
    private String contactNumber;
    private String address;

    // Creating Constructor
    public Contact() {};

    public Contact(String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    // getter and setter

    // getter
    public String getName() {
        return this.name;
    }

    public String getContactNumber() {
        return this.contactNumber;
    }

    public String getAddress() {
        return this.address;
    }

    // setter
    public void setName(String name) {
        this.name = name;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    // method
    public void printInfo() {
        if (contactNumber != null && address != null ) {
            System.out.println("Contact Info:");
            System.out.println("---------------------------------");
            System.out.println("Name: " + getName());
            System.out.println("Registered Number: " + getContactNumber());
            System.out.println("Address: " + getAddress());
            System.out.println("---------------------------------" + "\n");
        }
        else {
            System.out.println("Phonebook is empty. Please register one!");
        }
    }
}
