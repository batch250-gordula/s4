package com.zuitt.example;

public class Car {

    // Access Modifier
    // These are used to restrict the scope of a class, constructor, variable, method or data member.

    // FOUR Types of Access Modifier
        //1. Default (no keyword)
        //2. Private
        //3. Protected
        //4. Public

    // Class creation
    // FOUR Parts of Class creation
        //1. Properties - refers to characteristics of an object; also know as variable.

    private String name;
    private String brand;
    private int yearOfMake;

    // This Driver class - makes additional component of a car.
    private Driver driver;

        //2. Constructor - is used to create/instantiate an object.

            //a. empty constructor - creates object that doesn't have any arguments/parameters. Also referred as default constructor. (null or 0)
        public Car() {
            // to set a default value upon instantiation.
            this.yearOfMake = 2000;
            // whenever a new car is created, it will have a driver named "Alejandro".
            this.driver = new Driver("Alejandro");
        };

            //b. parameterized constructor - creates an object with argument/parameters.
                // also use as a template for instantiation for new object
        public Car(String name, String brand, int yearOfMake) {
            this.name = name;
            this.brand = brand;
            this.yearOfMake = yearOfMake;
            this.driver = new Driver("Alejandro");
        };

        //3. Getters and Setters - get and set the values of each property of an object.

            // Getters - retrieves the value of instantiated object
        public String getName() {
            return this.name;
        }

        public String getBrand() {
            return this.brand;
        }

        public int getYearOfMake() {
            return this.yearOfMake;
        }

        public String getDriverName() {
            return this.driver.getName();
        }

        // Setters - used to change the default value of an instantiated object.

        public void setName(String name) {
            this.name = name;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public void setYearOfMake(int yearOfMake) {
            // can also be modified to add validation
            if(yearOfMake <= 2022) {
                this.yearOfMake = yearOfMake;
            }
        }

        public void setDriver(String driver) {
            // This will invoke the setName() method of the Driver class.
            this.driver.setName(driver);
        }

        //4. Methods - functions that an object can perform (actions). This is optional.

        public void drive() {
            System.out.println("This car is running. Vrrrooomm, Vrroomm!");
        }

}
