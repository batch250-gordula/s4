package com.zuitt.example;
// Child class of Animal
    // "extends" keyword is used to inherit properties and methods of the parent class.
public class Dog extends Animal {

    // defining properties
    private String breed;

    // creating Constructor
    public Dog() {
        // to have a direct access with the original constructor(parent class)
        super();
        this.breed = "Chihuahua";
    };

    // parameterized Constructor
    public Dog(String name, String color, String breed) {
        // super(name, color); - properties coming from the Animal(parent) class
        super(name, color);
        this.breed = breed;
    }

    // getter and setter
    public String getBreed() {
        return this.breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    // method/function
    public void speak() {
        System.out.println("Woof, woof!");
    }

    public void call() {
//        super.call();
        System.out.println("Hi my name is " + this.name + ", I am a dog.");
    }


}
