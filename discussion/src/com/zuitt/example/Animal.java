package com.zuitt.example;
// Parent class

public class Animal {

    // Defining properties/description
    protected String name;
    private String color;

    // Creating Constructor
    // default
    public Animal() {};

    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    };

    // getter and setter
    // getter (has return)
    public String getName() {
        return this.name;
    }

    public String getColor() {
        return this.color;
    }

    // setter (no return) - use void
    public  void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    // method/function
    public void call() {
        System.out.println("Hi my name is " + this.name);
    }

}
